$(document).ready(function(){
    $(function() {
        var url = window.location.href;
    
        
        $("#nav a").each(function() {
            if (url == (this.href)) {
                $(this).closest("li").addClass("active");
            }
        });
    });

    $("li").mouseleave(function(){
      $("li").addClass("contract");
    });

    $(".inbox").css("color", "white");

    if ($(window).width() >=768 ) {
    $( ".box2" ).mouseenter(function() {
        $( ".inbox" ).animate({
          width: "20vw",
          backgroundColor: "#f0f0eb",
          color: "#424240"
        }, 1500 );
      });
      $( ".box2" ).mouseleave(function() {
        $( ".inbox" ).animate({
          width: "100%",
          backgroundColor: "#f02e2e",
          color: "white"
        }, 1500 );
      }); 
    }

    if ($(window).width() <=768 ) {
        $(".cl-nav").click(function(){
            $("#nav").toggle("fast");
        });
        $("li a").click(function(){
            $("#nav").hide("fast");
        });
    }
});